---
title: "About me"
date: 2024-01-01
description: "This page contains everything interesting about me."
showPagination: false
---

## About me

Nellanie Brook, born in 2000, lives in the heart of Nuremberg after a
childhood in the countryside. As a Master's student of psychology and
author, she turns her great passions into a profession. During her exam
periods, she likes to procrastinate by watching exciting series about
secret agents, going for walks, and drinking matcha lattes in her favorite
café. In her next life, she would like to become a therapy spider or a fly
agaric.
