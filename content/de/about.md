---
title: "Über mich"
date: 2024-01-01
description: "Diese Seite enthält alles wissenswerte über mich."
showPagination: false
---

## Über mich

Nellanie Brook, geboren im Jahr 2000, lebt nach einer Kindheit auf dem Land im Herzen von Nürnberg. Als angehende
Psychologin und Autorin macht sie ihre großen Leidenschaften zum Beruf. In ihren Klausurenphasen prokrastiniert sie am
liebsten mit spannenden Serien über Geheimagenten, Spaziergängen und guten Gesprächen. In ihrem nächsten Leben möchte
sie gern eine Therapiespinne oder ein Fliegenpilz werden.
