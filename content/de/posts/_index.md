---
title: "Blog"
---

Hier findet ihr meine Gedanken zum Schreiben, zum Psychologiestudium und zu allen anderen Themen, die sich in meinem
Kopf herumtreiben, wenn ich eigentlich für die nächste Klausur lernen sollte.
