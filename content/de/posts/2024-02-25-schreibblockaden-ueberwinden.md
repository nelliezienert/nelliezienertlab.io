---
title: "Schreibblockaden überwinden"
date: 2024-02-25
draft: false
summary: "Test"
---

## Schreibblockaden überwinden: Warum mir die besten Ideen beim Kaffeekochen kommen

Das Schreibprogramm ist offen, der weiße Bildschirm brennt sich wie ein Standbild in meine Netzhaut ein und dort, wo die ersten Zeilen meines neuen Kapitels stehen sollten, verspottet mich seit Minuten die blinkende Einfügemarke. An manchen Tagen versuche ich verzweifelt auch nur einen einzigen brauchbaren Satz auf die leere Seite zu bekommen, bis ich schließlich frustriert den Laptop zuknalle. Schreibblockaden rauben Zeit und Nerven, dabei können wir Autor\*innen viel von ihnen mitnehmen. Etwa, dass wir mal wieder eine Pause brauchen. Oder dass wir das gesamte letzte Kapitel über den Haufen werfen sollten.  

## Schreibblockaden – oder auch die Gretchenfrage der Schreiberlinge

„Nun sag’, wie hast du’s mit den Schreibblockaden?“ Als ich auf Social Media das erste Mal über Schreibblockaden berichtete, wurde ich von einer anderen Autorin belehrt, dass es diese überhaupt nicht gäbe. Schreibblockaden seien keine Hemmungen im Kopf, sondern lediglich ein Ausdruck davon, dass mit dem Text etwas nicht stimme, ein Anlass, sich auf Fehlersuche zu begeben. Genauso wie viele andere Autor\*innen habe auch ich bereits die Erfahrung gemacht, dass hinter Schwierigkeiten beim Schreiben oft Unstimmigkeiten im Plot stecken. Wenn die Worte nicht aufs Papier wollen, ist es Zeit, sich auf Detektivsuche zu begeben. Gibt es Logikfehler in meinem Text? Gibt es Handlungsstränge, die ich nicht gründlich genug geplant habe? Manchmal hilft es, ganze Kapitel zu streichen, umzuschreiben oder eine ganz andere Erzählperspektive einzunehmen. Anders als einige meiner Kolleg\*innen glaube ich allerdings, dass sich das Problem manchmal nicht im Text versteckt, sondern mit Zornesfalte (hey, ich kenne das schließlich auch!) vor dem Bildschirm sitzt.

## Warum Psycholog\*innen Kaffeepausen lieben

Manchmal deuten Schreibblockaden auch darauf hin, dass wir eine dringende, langersehnte Auszeit brauchen. Oder wie ich gern sage: Wer schreiben möchte, muss den Schreibtisch verlassen. Das habe ich nicht in einem Schreibworkshop, sondern in meinem Psychologiestudium gelernt. Mein damaliger Professor für Persönlichkeitspsychologie hat uns das Vier-Phasen-Modell des kreativen Prozesses vorgestellt und dafür gesorgt, dass ich meine Kaffeepausen nie wieder mit schlechtem Gewissen abhalten werde, egal wie viel Arbeit sich auf dem Schreibtisch stapelt. Unser Gehirn braucht Pausen. Genauer gesagt, nimmt das Vier-Phasen-Modell an, dass unser Verstand mit einer unbewussten Problemlösung beginnt, sobald wir uns ganz anderen Themen zuwenden. Während wir Kaffee trinken, spazieren gehen oder unter die Dusche springen, arbeitet unser Gehirn fleißig weiter, verknüpft Assoziationen, webt Gedankenstränge, bis es uns schließlich mit einem Geistesblitz überrascht. Heureka! Der alles verändernde Dialog, der geniale Plottwist, den wir am Schreibtisch verzweifelt ausarbeiten wollten, ist plötzlich da.

## Gott will nicht, dass ich schreibe. Ich aber, ich muss 

Wenn die Worte im Kopf feststecken und jeder neu geschriebene Satz auf noch mehr Verzweiflung stößt, ist es höchste Zeit, dass wir das Schreiben für einen Augenblick pausieren und uns fragen, wo genau das Problem liegt. Neben Schwachstellen im Plot und geistige Überanstrengung können auch andere Faktoren wie Perfektionismus, mangelnde Vorbereitung oder Versagensängste eine Rolle spielen. Im Internet haben bereits viele erfahrene Autor\*innen ihre Tipps geteilt, um eine Schreibblockade erfolgreich zu überwinden. Das Problem: Selbst die besten Ratschläge bewirken keine Wunder. Ich persönlich halte Kreativität für etwas Magisches, das sich zwar zum Teil durch psychologische Theorien beschreiben lässt, doch auf eine zauberhafte Art und Weise nicht vollständig erklärbar ist. Immerhin wurden selbst die größten Schreibikonen wie Franz Kafka und Sylvia Plath regelmäßig von Schreibblockaden geplagt. Manchmal bleibt einem nichts anderes übrig, als sich durch den Text durchzubeißen. Mit dem tröstenden Gedanken, dass auch jede Schreibblockade nur vorübergehend ist – und wir auch bald wieder Tage haben werden, an denen wir mit dem Tippen gar nicht mehr hinterherkommen, da die Worte geradezu aus uns heraussprudeln.
