---
title: "Nordrhein-Westfalen - Krefeld"
date: 2024-05-18
---

## Eckentaler Autorenlesung

**Datum**: 18. Mai 2024</br>
**Uhrzeit**: 15:00-17:00 Uhr</br>
**Ort**: Thalia Krefeld, Hochstraße 96-100, 47798 Krefeld</br>
**Inhalt**: Signierstunde und Mini-Lesung zu meinem Young-Adult-Roman "Auf
mich wartet das Nimmerland"  
**Eintritt**: Kostenlos

### Teaser

Ihr kommt aus der Nähe von Krefeld und habt Lust, euch ein signiertes
Exemplar von Nimmerland abzuholen oder mit mir über das Schreiben zu
plaudern? Nur wenige Tage nach dem Release von „Auf mich wartet das
Nimmerland“ bin ich bei Thalia Krefeld zu Gast, um meinen Debütroman mit
einer kleinen Leseprobe vorzustellen. Kommt doch gern vorbei – und verpasst
nicht die Gelegenheit, euch eine kostenlose Postkarte von Annabella und
Florian mitzunehmen!

### Wie komme ich zur Lesung?

{{< openstreetmap mapName="2024-05-18-lesung-krefeld_1066676" >}}
