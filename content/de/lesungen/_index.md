---
title: "Lesungen"
showDate: false
showAuthor: false
showPagination: false
---

Hey, ich bin Nellanie Brook! Meine Welt dreht sich um Geschichten, die ich
für euch niederschreibe. Wenn ihr neugierig auf meinen Roman "Auf mich
wartet das Nimmerland" seid, dann lasst uns in Kontakt treten! Lesungen
sind für mich nicht nur Momente des Vorlesens, sondern lebendige
Begegnungen mit euch. Ich komme gerne zu euch, sei es in Jugendtreffs oder
an Schulen, um die Magie der Worte mit euch gemeinsam aufleben zu lassen
und mit euch über das Thema Essstörungen zu reden. Schreibt mir jederzeit
unter info@nellaniebrook.de und gemeinsam lassen wir die Seiten meiner
Bücher zum Leben erwachen.

Ich freue mich darauf, euch bei einer meiner Lesungen zu begegnen!
