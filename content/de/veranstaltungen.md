---
title: "Veranstaltungen"
showDate: false
showAuthor: false
showPagination: false
---

Ich freue mich sehr stark auf Lesungen eingeladen zu werden. Wenn ihr Interesse habt mich zu buchen, dann kontaktiert
mich einfach unter <nellanie@nellaniebrook.de>.
