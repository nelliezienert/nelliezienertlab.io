---
title: "Impressum"
showDate: false
showAuthor: false
showPagination: false
showWordCount: false
showReadingTime: false
---

Nellanie Brook</br>

c/o Postflex #8151</br>
Emsdettener Str. 10</br>
48268 Greven</br>

Keine Pakete oder Päckchen - Annahme wird verweigert!

## Kontakt

Telefon: +49 911 97195356</br>
E-Mail: <nellanie@nellaniebrook.de>

## Verbraucherstreitbeilegung/Universalschlichtungsstelle

Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
teilzunehmen.

Quelle: <https://www.e-recht24.de/impressum-generator.html>
