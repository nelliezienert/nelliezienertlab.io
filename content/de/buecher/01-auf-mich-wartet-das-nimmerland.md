---
title:  "Auf mich wartet das Nimmerland"
date: 2024-05-15
---

{{< figure src="buecher/01-nimmerland/buch.png" alt="Buchcover zu Auf mich wartet das Nimmerland" class="center">}}

## Klappentext

Annabella ist magersüchtig. Statt für ihren Platz an einer professionellen Ballettschule zu trainieren, soll sie in einer Klinik für Essstörungen gesund werden. Anstelle von Spitzentanz und Spagat heißt es jetzt Bettruhe und Gruppentherapie. Annabella ist sich sicher: Selbst wenn sie die Magersucht besiegt, wird sie nie wieder auf einer Bühne stehen können. Doch dann lernt sie Florian und seine verrückte Theatergruppe kennen und muss eine Entscheidung treffen, die ihr ganzes Leben verändern wird: Krank sein oder für die Liebe kämpfen?

## Links

- [Verlagswebseite](https://www.vajona.de/auf-mich-wartet-das-nimmerland)
- Amazon: [Link zum Buch](https://www.amazon.de/Auf-mich-wartet-das-Nimmerland/dp/398718292X)
- Thalia: [Link zum Buch](https://www.thalia.de/shop/home/suggestartikel/A1070875638)

## Charakterkarte

{{< figure src="buecher/01-nimmerland/charakterkarte.jpeg" alt="Charakterkarte zu Auf mich wartet das Nimmerland" class="center">}}
