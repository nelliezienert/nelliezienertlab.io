## My author website

Website URLs:

- https://nelliezienert.gitlab.io/
- https://nellaniebrook.de/

This repository contains the source code for my author's website. The site is built with Hugo and the Blowfish Theme for
now.

## Useful links

Hugo:

- [Website](https://gohugo.io/)

Blowish Theme:

- [Hugo Theme Repository](https://themes.gohugo.io/themes/blowfish/)
- [Website](https://blowfish.page/)
- [Docs](https://blowfish.page/docs/)

OSM Thema:

- [Guide to add to your blog](https://www.thecoffeemachine.net/writing/adding-maps-to-hugo-blogs-with-osm/)
- [GitHub Repo](https://github.com/hanzei/hugo-component-osm/tree/master)
